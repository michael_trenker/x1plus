import java.lang.reflect.Field;
import java.util.Date;

public class UmsatzDoa {
    String externData;
    String invoiceDate;
    String invoiceNumber;
    String invoiceBrutTotal;
    String invoiceNetTotal;
    String invoiceNetInst;
    String invoiceNetRemain;
    String invoiceCurrency;
    String invoiceVat;
    String invoicePeriod;
    String invoicePrePaidTrans;
    String invoiceCountry;
    String invoiceCountryToken;
    String customerId;
    String customerName;
    String customerRoot;
    String customerUid;
    String customerCostCenter;

    //reads all data from input csv and assigns it to above attributes
    public UmsatzDoa(String data) throws IllegalAccessException {
        String[] values = data.split(";");
        int i = 0;
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            field.set(this,values[i]);
            i++;
        }

        System.out.println(this);
    }

    public String getexternData() {
        return externData;
    }


    public String getInvoiceDate() {
        return invoiceDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getInvoiceBrutTotal() {
        return invoiceBrutTotal;
    }

    public String getInvoiceNetTotal() {
        return invoiceNetTotal;
    }

    public String getInvoiceNetInst() {
        return invoiceNetInst;
    }

    public String getInvoiceNetRemain() {
        return invoiceNetRemain;
    }

    public String getInvoiceCurrency() {
        return invoiceCurrency;
    }

    public String getInvoiceVat() {
        return invoiceVat;
    }

    public String getInvoicePeriod() {
        return invoicePeriod;
    }

    public String getInvoicePrePaidTrans() {
        return invoicePrePaidTrans;
    }

    public String getInvoiceCountry() {
        return invoiceCountry;
    }

    public String getInvoiceCountryToken() {
        return invoiceCountryToken;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCustomerRoot() {
        return customerRoot;
    }

    public String getCustomerUid() {
        return customerUid;
    }

    public String getCustomerCostCenter() {
        return customerCostCenter;
    }

    @Override
    public String toString() {
        return "UmsatzDoa{" +
                "invoiceDate=" + invoiceDate +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", invoiceBrutTotal=" + invoiceBrutTotal +
                ", nvoiceNetTotal=" + invoiceNetTotal +
                ", invoiceNetInst=" + invoiceNetInst +
                ", invoiceNetRemain=" + invoiceNetRemain +
                ", nvoiceCurrency='" + invoiceCurrency + '\'' +
                ", invoiceVat='" + invoiceVat + '\'' +
                ", invoicePeriod='" + invoicePeriod + '\'' +
                ", invoicePrePaidTrans='" + invoicePrePaidTrans + '\'' +
                ", invoiceCountry='" + invoiceCountry + '\'' +
                ", invoiceCountryToken='" + invoiceCountryToken + '\'' +
                ", customerId=" + customerId +
                ", customerName='" + customerName + '\'' +
                ", customerRoot='" + customerRoot + '\'' +
                ", customerUid='" + customerUid + '\'' +
                ", customerCostCenter=" + customerCostCenter +
                '}';
    }
}
