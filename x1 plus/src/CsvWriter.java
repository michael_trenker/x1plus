import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class CsvWriter {


    static List<UmsatzDoa> umsatzDoaLinkedList = new LinkedList<>();
    static StringBuilder sb = new StringBuilder();





    public static void main(String[] args) {
    //TODO Implement basic interface to define input and output Path (Outpust MUST be "datevFormatFile.csv"
        try (PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File("test.csv")), StandardCharsets.UTF_8))) {
            try (BufferedReader br = new BufferedReader (new InputStreamReader(new FileInputStream("201907_Umsatz.csv"), "UTF8"))) {
                String line;
                //reads input data and cast them into object
                while ((line = br.readLine()) != null) {
                    umsatzDoaLinkedList.add(new UmsatzDoa(line));
                }

            }

            //writes metaData

            METAData data = new METAData();


            for (Field field : data.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                String name = field.getName();
                Object value = field.get(data);
                System.out.printf("%s: %s%n", name, value);
                sb.append(value);
                sb.append(';');
            }
            sb.append('\n');
            sb.append(HeadLines.getHeadlines());




            //writes the actual data
            for (int i = 0; i < umsatzDoaLinkedList.size(); i++) {
               writeUmsatz(umsatzDoaLinkedList.get(i));
            }


            writer.write(sb.toString());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("done!");


    }


    static void writeUmsatz(UmsatzDoa d) throws ParseException {

        //wirtes data, because of lots of empty field very ugly solution

        List<FieldColumn> fieldList = new LinkedList<FieldColumn>();
        String[] headline = HeadLines.getHeadlines().split(";");

        for(int i = 0; i < headline.length;i++){

            FieldColumn f = new FieldColumn(headline[i]);
            System.out.println(f.fieldname);
            //System.out.println(f);
            fieldList.add(f);

        }



        for(int i = 0; i < headline.length;i++){

            if(fieldList.get(i).fieldname.equalsIgnoreCase("Umsatz(ohne Soll/Haben-Kz)")){
                fieldList.get(i).setValue(d.invoiceDate);
                System.out.println("Here is Value " + fieldList.get(i).getValue());
            }
            fieldList.get(i).value = ((fieldList.get(i).fieldname.equalsIgnoreCase("Umsatz(ohne Soll/Haben-Kz)"))?  d.invoiceBrutTotal:fieldList.get(i).value);
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("//Soll/Haben Kennzeichen")? getSoll_Haben_Kennzeichen(d.getInvoiceBrutTotal()).replaceAll("[\"]",""):fieldList.get(i).value;
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("Konto")?  d.externData:fieldList.get(i).value;
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("Gegenkonto(ohne BU-Schlüssel)")?  "4400":fieldList.get(i).value;
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("Belegdatum")?  d.getInvoiceDate().substring(1,3)+ d.getInvoiceDate().substring(4,6):fieldList.get(i).value;
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("Buchungstext")?  "AR " + d.customerId.replaceAll("[\"]","") + d.customerName.replaceAll("[\"]",""):fieldList.get(i).value;
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("KOST1-Kostenstelle")? d.customerCostCenter:fieldList.get(i).value;
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("Herkunft-Kz")? "Mavoco":fieldList.get(i).value;
            fieldList.get(i).value = fieldList.get(i).fieldname.equalsIgnoreCase("Festschreibung")? "0":fieldList.get(i).value;

            //System.out.println(i);

            sb.append(fieldList.get(i));

            sb.append(";");
           // System.out.println(fieldList.get(i).value);
        }


        sb.append('\n');








    }







    static String getSoll_Haben_Kennzeichen(String s){
        if (s.contains("-")) {
            return ("H");
        } else {
            return ("S");
        }


    }

    static String  getLowestDate() throws ParseException {
        SimpleDateFormat objSDF = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat format = new SimpleDateFormat("\"dd.MM.yyyy\"");
        Date date2 = objSDF.parse("12.10.2099");;

        for (int i = 0; i < umsatzDoaLinkedList.size(); i++) {
            Date date1 = format.parse(umsatzDoaLinkedList.get(i).invoiceDate);

            if (date1.compareTo(date2) < 0) {
                date2 = date1;

            }

        }
        SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
        return formatter.format(date2);

    }




    static String  getHighestDate() throws ParseException {
        SimpleDateFormat objSDF = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat format = new SimpleDateFormat("\"dd.MM.yyyy\"");
        Date date2 = objSDF.parse("12.10.1999");;
        for (int i = 0; i < umsatzDoaLinkedList.size(); i++) {
            Date date1 = format.parse(umsatzDoaLinkedList.get(i).invoiceDate);
            if (date1.compareTo(date2) > 0) {
                date2 = date1;

            }

        }
        SimpleDateFormat formatter= new SimpleDateFormat("yyyyMMdd");
        return formatter.format(date2);

    }

}
